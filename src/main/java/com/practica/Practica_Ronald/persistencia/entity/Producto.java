package com.practica.Practica_Ronald.persistencia.entity;



import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name ="productos")
public class Producto {


    //anotacion se pone porque es una clave primaria sencilla de la tabla productos
    //se va a generar automaticamente al crear producto
    @Id
    //se le añade una estategia porque esta columna le da identidad a la tabla producto
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    //nombre de la columna
    @Column(name = "id_producto")
    private Integer idProducto;

    private String nombre;
    @Column(name = "id_categoria")
    private Integer idCategoria;

    @Column(name = "codigo_barras")
    private String codigoBarras;

    @Column(name = "precio_venta")
    private Double precioVenta;

    @Column(name = "cantidad_stock")
    private Integer cantidadStock;

    private Boolean estado;


    @ManyToOne
    @JoinColumn(name="id_categoria",insertable = false,updatable = false)
    private Categorias categoria;


    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public Double getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(Double precioVenta) {
        this.precioVenta = precioVenta;
    }

    public Integer getCantidadStock() {
        return cantidadStock;
    }

    public void setCantidadStock(Integer cantidadStock) {
        this.cantidadStock = cantidadStock;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
}