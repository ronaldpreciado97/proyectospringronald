package com.practica.Practica_Ronald;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaRonaldApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaRonaldApplication.class, args);
	}

}
