package com.practica.Practica_Ronald.web.Controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Saludar")
public class Controlador {

    @GetMapping("/hola")
    public String Saludar(){
        return "Hola , es mi primera aplicacion en springboot";
    }
}
